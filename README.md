This fork aims to make VICE 3.4 compliant with UK copyright law, under the terms of VICE's GNU General Public License (https://vice-emu.sourceforge.io/index.html#copyright).
To this end, firmware ROM images have been stripped out and a script has been provided to make it easy for users to integrate legally obtained ROMs. 


The Commodore firmware images are owned and licensed by Cloanto. 
The ROMs required by this VICE fork should be obtained as part of Cloanto's C64 Forever Express (https://www.c64forever.com/download/)


For convenience, Linux users are advised to install C64 Forever using Wine 5.0 or above, which has full MSI installer support (see https://www.winehq.org/news/2020012101). After installing this, locate /users/Public/Documents/CBM Files/Shared/rom/ of your Windows system drive.

Copy the rom directory to the vice-3.5-copyright-compliant directory and run copyrom.sh


*Steps to install*

1. sudo apt install autoconf flex byacc x65 libsdl2* libpcap-dev
2. Extract/clone vice-3.5-copyright-compliant
3. Install C64 Forever
4. Copy C64 Forever's /users/Public/Documents/CBM Files/Shared/rom/ directory into the vice-3.5-copyright-compliant directory
5. cd vice-3.5-copyright-compliant
6. ./copyrom.sh
7. ./autogen.sh
8. ./configure --enable-sdlui2 --without-oss --enable-ethernet --disable-catweasel --without-pulse --enable-x64
9. make -j $(nproc)
10. sudo make install

Note that ROMs are not currently available via Cloanto for the Commodore PET, C64DTV and SCPU64. 
This fork includes an SCPU64-compatible ROM created by the VICE team.

----
Anti-deletion line, please ignore.
